After you download or clone the repo do the following:

Running the Angular App:
-Install NodeJS and NPM from https://nodejs.org.
-Install the Angular CLI using the command: npm install -g @angular/cli
-Start the app with the Angular CLI command ng serve --open this will compile the Angular app and automatically launch it in the browser on the URL http://localhost:4200.

Running the ASP.NET Core 5 API:
-Install the .NET Core SDK from https://www.microsoft.com/net/download/core.
-Open the project in visual studio and run it you should see the message Now listening on: http://localhost:4000